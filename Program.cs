﻿using System;
using OpenQA.Selenium;
using System.IO;
using OpenQA.Selenium.Support.UI;

namespace Тест_дизайн_Яндекс_почта
{
    class Program
    {
        static IWebDriver Browser = new OpenQA.Selenium.Opera.OperaDriver();
        
        static IWebElement element;
        static WebDriverWait ww = new WebDriverWait(Browser, TimeSpan.FromMinutes(0.5));
        static string login = "uchakdlateke";
        static string password = "TeKepoYaPo37shT";
        static string text;
        static void Main(string[] args)
        {
            //Создание папки со скриншотами,описанием ошибок и проверками, если её нет
            DirectoryInfo dirInfo = new DirectoryInfo(@"Сheck");
            if (!dirInfo.Exists)
                dirInfo.Create();
            File.Delete(@"Сheck\test.txt");
            File.Create(@"Сheck\test.txt").Close();
            //Получение логина и пароля
            /*if (args.Length != 0)
            {
                login = args[0];
                password = args[1];
                Console.WriteLine("Логин и пароль получены");
            }*/
            //Добавить текст для письма из файлов
            AddInfo();
            Browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(2);
            Browser.Manage().Window.Maximize();      
            //Открыть сайт почты
            Open();
            //Зайти в существующую почту
            Login();
            //Отправить письмо
            Send();
            Browser.Quit();
        }
        //Добавтиь логин, пароль и текст для письма из файлов
        static void AddInfo()
        {
            using (StreamReader sr = new StreamReader(@"Files/text.txt", System.Text.Encoding.Default))
            {
                do { text = sr.ReadLine(); }
                while (sr.ReadLine() != null);
                sr.Close();
            }
        }
        //Открыть сайт почты
        static void Open()
        {
            try
            {
                Browser.Navigate().GoToUrl("https://mail.yandex.ru/");
            }
            catch (Exception ex)
            {
                Testing("ERROR", ex);
            }
        }
        //Зайти в существующую почту
        static void Login()
        {
            try
            {
                element = Browser.FindElement(By.ClassName("HeadBanner-Button-Enter342"));
                element.Click();
                element = Browser.FindElement(By.Id("passp-field-login"));
                element.SendKeys(login);
                if (element.GetAttribute("Value") != login)
                    Testing("Строка 73: Проверка передачи логина: Ошибка!!!!!!!!!!", null);
                element = Browser.FindElement(By.ClassName("Button2_type_submit"));
                element.Click();
                element = Browser.FindElement(By.Id("passp-field-passwd"));
                element.SendKeys(password);
                if (element.GetAttribute("Value") != password)
                    Testing("Строка 79: Проверка передачи пароля: Ошибка!!!!!!!!!!", null);
                element = Browser.FindElement(By.ClassName("Button2_type_submit"));
                element.Click();
            }
            catch (Exception ex)
            {
                Testing("ERROR", ex);
            }
        }
        //Отправить письмо
        static void Send()
        {
            try
            {
                element = Browser.FindElement(By.ClassName("js-main-action-compose"));
                element.Click();
                element = Browser.FindElement(By.ClassName("composeYabbles"));
                element.SendKeys("uchakdlateke@yandex.ru");
                if (Browser.FindElements(By.XPath("//*[contains(text(), 'uchakdlateke@yandex.ru')]")).Count != 0)
                { }
                else
                    Testing("Строка 98: Проверка передачи почты: Ошибка!!!!!!!!!!", null);
                element = Browser.FindElement(By.ClassName("ComposeSubject-TextField"));
                element.SendKeys("Удлер Мария АВТОТЕСТЫ");
                if (Browser.FindElements(By.XPath("//*[contains(text(), 'Удлер Мария АВТОТЕСТЫ')]")).Count != 0)
                { }
                else
                    Testing("Строка 104: Проверка передачи названия темы: Ошибка!!!!!!!!!!", null);
                element = Browser.FindElement(By.ClassName("cke_contents_ltr"));
                element.SendKeys(text);
                element = Browser.FindElement(By.ClassName("WithUpload-FileInput"));
                element.SendKeys(@"C:\Users\2015\Documents\Visual Studio 2015\Projects\Тест дизайн Яндекс почта\Тест дизайн Яндекс почта\bin\Debug\Files\podbornj.jpg");
                element.SendKeys(@"C:\Users\2015\Documents\Visual Studio 2015\Projects\Тест дизайн Яндекс почта\Тест дизайн Яндекс почта\bin\Debug\Files\text.txt");
                element = Browser.FindElement(By.ClassName("ComposeControlPanel-SendButton "));
                element.Click();
                element = Browser.FindElement(By.ClassName("ComposeConfirmPopup-Button_action"));
                element.Click();
                element = Browser.FindElement(By.ClassName("ComposeControlPanel-SendButton "));
                element.Click();
                element = Browser.FindElement(By.ClassName("ComposeDoneScreen-Actions"));
                element.Click();
                Browser.Navigate().Refresh();
                if (Browser.FindElements(By.XPath("//*[contains(text(), text)]")).Count != 0)
                { }
                else
                    Testing("Строка 122: Проверка получения письма: Ошибка!!!!!!!!!!", null);
            }
            catch (Exception ex)
            {
                Testing("ERROR", ex);               
            }
        }
        static void Testing(string result, Exception ex)
        {
            using (StreamWriter sw = new StreamWriter(@"Сheck\test.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(result);
                if (ex != null)
                {
                    sw.WriteLine(ex);
                    string timestamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    Screenshot image = ((ITakesScreenshot)Browser).GetScreenshot();
                    string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                    _filePath = Directory.GetParent(_filePath).FullName;
                    _filePath = Directory.GetParent(_filePath).FullName;
                    image.SaveAsFile(@"Сheck\" + timestamp + ".png");
                    String PageSource = Browser.PageSource;
                    System.IO.File.WriteAllText(@"Сheck\" + timestamp + "filename.html", PageSource);
                    Browser.Quit();
                }
                sw.Close();
            }            
        }
    }
}
